#include <iostream>

using namespace std;
const double eps = 0.0000000001;
class Matrix{
    int n, m;
    double **mas;
    public:
        Matrix()
        {
            this->n = 1;
            this->m = 1;
            mas = new double*[n];
            mas[0] = new double[m];
            mas[0][0] = 0.0; 
        }
        Matrix(int n, int m)
        {
            this->n = n;
            this->m = m;
            mas = new double*[n];
            for(int i = 0; i < n; i++)
            {
                mas[i] = new double[m];
            }
            for(int i = 0; i < n; i++)
                for(int j = 0; j < m; j++)
                    mas[i][j] = 0.0;
        }
        Matrix(int n)
        {
            this->n = n;
            this->m = n;
            mas = new double* [n];
            for(int i = 0; i < n; i++)
                mas[i] = new double [n];
            for(int i = 0; i < n; i++)
                for(int j = 0; j < n; j++)
                    mas[i][j] = 0.0;
        }
        Matrix(double x)
        {
            this->n = 1;
            this->m = 1;
            mas = new double*[n];
            mas[0] = new double[m];
            mas[0][0] = x;
        }
        Matrix(const Matrix &a)
        {
            this->n = a.n;
            this->m = a.m;
            mas = new double* [n];
            for(int i = 0; i < n; i++)
                mas[i] = new double [m];
            for(int i = 0; i < n; i++)
                for(int j = 0; j < m; j++)
                    mas[i][j] = a.mas[i][j];
        }
        ~Matrix()
        {
            for(int i = 0; i < n; i++)
                delete[] mas[i];
            if(this->n > 1 && this->m > 1)
                delete[] mas;
        }
        double& elem(int i = 0, int j = 0) 
        {
            return mas[i][j];
        }
        void set(int i, int j, double val)
        {
            mas[i][j] = val;
        }
        
        int checkcolumn(int i, int start)
        {
            for(int j = start; j < m; ++j)
                if(mas[j][i] != 0.0)
                {
                    return j;
                }
            
            return -1;
        }

        void swap_string(int i, int j)
        {
            double * tmp = new double [n];
            if(i == j) return;
            for(int k = 0; k < n; ++k)
            {
                tmp[k] = mas[i][k];
                mas[i][k] = mas[j][k];
                mas[j][k] = tmp[k];
            }
            delete[] tmp;
        }

        void minus_string(int i, int j, double alpha)
        {
            for(int k = 0; k < n; ++k)
            {
                mas[i][k] -= alpha * mas[j][k];
                if(abs(mas[i][k]) < eps)
                    mas[i][k] = 0.0;
            }
        }

        Matrix &operator * (double x)
        {
            static Matrix a(n, m);
            for(int i = 0; i < n; i++)
                for(int j = 0; j < m; j++)
                    a.set(i, j, mas[i][j]);
            for(int i = 0; i < n; i++)
                for(int j = 0; j < m; j++)
                    a.mas[i][j] *= x;
            return a;
        }
        const Matrix &operator *= (double x)
        {
            for(int i = 0; i < n; i++)
                for(int j = 0; j < m; j++)
                    mas[i][j] *= x;
            return *this;
        }
        Matrix &operator = (const Matrix &);
        friend std::ostream& operator<< (std::ostream &out, const Matrix &matrix);
        Matrix &operator + (const Matrix &);
        Matrix &operator += (const Matrix &);
        Matrix &operator - (const Matrix &);
        Matrix &operator -= (const Matrix &);
        Matrix &operator * (const Matrix &);
        Matrix &operator *= (const Matrix &);
        bool operator == (const Matrix &);
        bool operator != (const Matrix &);
        friend Matrix &operator - (const Matrix &);
        Matrix operator [] (int i)
        {
            if(i >= 0 && i < n)
            {
                static Matrix tmp(1, m);
                for(int j = 0; j < m; j++)
                    tmp.set(0, j, mas[i][j]);
                return tmp;
            }
            else if(i >= 0 && i < m)
            {
                static Matrix tmp(n, 1);
                for(int j = 0; j < n; j++){
                    tmp.set(j, 0, mas[j][i]);
                }
                return tmp;
            }
            else
            {
                std::cout << "Error of index!" << std::endl;
                return *this;
            }
        }
        
};

Matrix &Matrix::operator + (const Matrix &b)
{
    if(n == b.n && m == b.m)
    {
        static Matrix a(n, m);
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                a.set(i, j, mas[i][j]);
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                a.mas[i][j] += b.mas[i][j];
        return a;    
    }
    else
    {
        cout << "Irregular size of matrix" << endl;
        return *this;
    }
}

Matrix &Matrix::operator += (const Matrix &b)
{
    if(n == b.n && m == b.m)
    {
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                mas[i][j] += b.mas[i][j];
    }
    else
    {
        cout << "Irregular size of matrix" << endl;
    }
    return *this;
}

Matrix &Matrix::operator - (const Matrix &b)
{
    if(n == b.n && m == b.m)
    {
        static Matrix a(n, m);
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                a.set(i, j, mas[i][j]);
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                a.mas[i][j] -= b.mas[i][j];
        return a;    
    }
    else
    {
        cout << "Irregular size of matrix" << endl;
        return *this;
    }
}

Matrix &Matrix::operator -= (const Matrix &b)
{
    if(n == b.n && m == b.m)
    {
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                mas[i][j] -= b.mas[i][j];
    }
    else
    {
        cout << "Irregular size of matrix" << endl;
    }
    return *this;
}

Matrix &Matrix::operator * (const Matrix &b)
{
    if(m == b.n)
    {
        static Matrix a(n, b.m);
        for(int i = 0; i < n; i++)
            for(int j = 0; j < b.m; j++)
                for(int k = 0; k < m; k++)
                    a.mas[i][j] += mas[i][k] * b.mas[k][j];
        return a;    
    }
    else
    {
        cout << "Irregular size of matrix" << endl;
        return *this;
    }
}

Matrix &Matrix::operator *= (const Matrix &b)
{
    if(m == b.n)
    {
        Matrix a(n, b.m);
        for(int i = 0; i < n; i++)
            for(int j = 0; j < b.m; j++)
                for(int k = 0; k < m; k++)
                    a.mas[i][j] += mas[i][k] * b.mas[k][j];
        for(int i = 0; i < n; i++)
                delete[] mas[i];
        if(this->n > 1 && this->m > 1)
            delete[] mas;
        this->m = b.m;
        mas = new double*[n];
        for(int i = 0; i < n; i++)
        {
            mas[i] = new double[m];
        }
        for(int i = 0; i < n; i++)
            for(int j = 0; j < m; j++)
                mas[i][j] = a.mas[i][j];
    }
    else
    {
        cout << "Irregular size of matrix" << endl;
    }
    return *this;
}

Matrix &Matrix::operator = (const Matrix &a)
        {
            if(n == a.n && m == a.m)
                for(int i = 0; i < n; i++)
                    for(int j = 0; j < m; j++)
                        mas[i][j] = a.mas[i][j];
            else
                cout << "Error of size" << endl;
            return *this;
        }

std::ostream& operator<< (std::ostream &out, const Matrix &a)
{
    for(int i = 0; i < a.n; i++)
    {
        for(int j = 0; j < a.m; j++)
            out << a.mas[i][j] << " ";
        out << " " << endl;
    }

    return out;
}

Matrix &operator - (const Matrix &a)
{
    static Matrix b(a.n, a.m);
    for(int i = 0; i < a.n; i++)
        for(int j = 0; j < a.m; j++)
            b.mas[i][j] = -a.mas[i][j];
    return b;
} 