#include "matrix.cpp"
#include <stdlib.h>
#include <time.h>

void Hessenberg(Matrix& A, int N)
{
    if(N != 1 && N != 2)
    {
        bool ans = true;
        for(int i = 0; i < N; ++i)
        {
            for(int j = i + 2; j < N; ++j)
                if(A.elem(i, j) != 0.0){
                    ans = false;
                    break;
                }
            if(!ans)
                break;
        }    
        if(ans)
            return;
        int start, i;
        for(i = 0; i < N - 1; ++i)
        {
            if((start = A.checkcolumn(i, i + 2)) >= 0)
                break;
        }
        if(start < 0) return;
        if(A.elem(i + 1, i) == 0.0) A.swap_string(i + 1, start);
        for(int j = i; j < N - 1; ++j)
        {
            if((start = A.checkcolumn(j, j + 2)) < 0)
                continue;
            for(int k = start; k < N; ++k)
            {
                A.minus_string(k, j + 1, A.elem(k, j) / A.elem(j + 1, j));
            }
        }

    }

}

int main()
{
    int N;
    cin >> N;
    srand (time(NULL));
    if(N <= 0){
        cout << "Fuck you, bitch" << endl;
        return 0;
    }
    Matrix A(N);
    for(int i = 0; i < N; ++i)
        for(int j = 0; j < N; ++j)
            A.elem(i, j) = rand() % 100;
    
    cout << "Ready? Usual form: " << endl;
    cout << A << endl;

    Hessenberg(A, N);

    cout << "Ready? Hessenberg(modified) form: " << endl;
    cout << A << endl;
    return 0;
}